/*
 * Copyright (c) 2011, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <xdc/std.h>
#include <xdc/cfg/global.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/knl/Thread.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/ipc/MultiProc.h>
#include <ti/ipc/rpmsg/VirtQueue.h>

#include <ti/xdais/dm/xdm.h>
#include <ti/sysbios/hal/Cache.h>

#include <ti/ipc/rpmsg/MessageQCopy.h>
#include <ti/srvmgr/NameMap.h>
#include <ti/resmgr/IpcResource.h>

#include "rdaemon_priv.h"

#define RDAEMON_PORT 43

static void rdaemon_main(uint32_t arg0, uint32_t arg1)
{
    MessageQCopy_Handle handle;
    UInt32 ep, rep;
    Uint16 dst;

    INFO("Creating rdaemon MessageQ...");

    dst = MultiProc_getId("HOST");

    /* Create the messageQ for receiving (and get our endpoint for sending). */
    handle = MessageQCopy_create(RDAEMON_PORT, &ep);
    NameMap_register("rpmsg-rdaemon", RDAEMON_PORT);

    INFO("Ready to receive requests");

    /* Dispatch loop */
    while (TRUE) {
        UInt16 len;
	struct rdaemon_msg_frame received_frame;
	UInt32 msg_type;
	UInt32 data;
        int ret;

        ret = MessageQCopy_recv(handle, &received_frame, &len, &rep, MessageQCopy_FOREVER);
        if (ret) {
            ERROR("MessageQ recv error: %d", ret);
            break;
        }

	if (received_frame.msg_type == RDAEMON_PING)
		INFO("PING : %d ", received_frame.data);

    }

    /* Teardown our side: */
    MessageQCopy_delete(&handle);
}

Bool rdaemon_init(void)
{
    Task_Params params;

    INFO("Creating rdaemon server thread...");

    /* Create rdaemon task. */
    Task_Params_init(&params);
    params.instance->name = "rdaemon-server";
    params.priority = Thread_Priority_ABOVE_NORMAL;
    Task_create(rdaemon_main, &params, NULL);

    return TRUE;
}
